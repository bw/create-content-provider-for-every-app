# Android5.1.1源码 - 为每个应用添加一个Content Provider

## 说明

修改PackageParser类，该类在文件"system/frameworks/base/core/java/android/content/pm/PackageParser.java"中。

PackageParser-orig.java文件保存的是PackageParser类的源码。

PackageParser-changed.java文件保存的是更改后的PackageParser类的源码，更改后的类源码将为每个应用添加一个Content Provider。